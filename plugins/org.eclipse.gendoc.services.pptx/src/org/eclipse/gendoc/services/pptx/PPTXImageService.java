/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.services.pptx;

import java.io.File;

import javax.xml.xpath.XPathExpressionException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gendoc.document.parser.documents.XMLParser;
import org.eclipse.gendoc.document.parser.documents.helper.UnitsHelper;
import org.eclipse.gendoc.document.parser.documents.helper.UnitsHelper.Unit;
import org.eclipse.gendoc.document.parser.pptx.PPTXDocument;
import org.eclipse.gendoc.document.parser.pptx.PPTXHelper;
import org.eclipse.gendoc.document.parser.pptx.PPTXParser;
import org.eclipse.gendoc.document.parser.pptx.XPathPptxUtils;
import org.eclipse.gendoc.documents.AbstractImageService;
import org.eclipse.gendoc.documents.IAdditionalResourceService;
import org.eclipse.gendoc.documents.IDocumentService;
import org.eclipse.gendoc.documents.ImageDimension;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.AdditionalResourceException;
import org.eclipse.gendoc.tags.ITag;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

//TODO: Provide a common ooxml abstract implementation of XLSXImageService
public class PPTXImageService extends AbstractImageService
{
	@Override
	public String manageImage(ITag tag, String imageId, String filePath, boolean keepH, boolean keepW, boolean maxH, boolean maxW) throws AdditionalResourceException {
		IDocumentService documentService = GendocServices.getDefault().getService(IDocumentService.class);
		PPTXDocument document = (PPTXDocument)documentService.getDocument();
		XMLParser parser = document.getXMLParser();
		if (!(parser instanceof PPTXParser))
			return "";

		IAdditionalResourceService additionalResourceService = documentService.getAdditionalResourceService();
		String imageFileName = additionalResourceService.getImageRelativePath(imageId);
		int index = imageFileName.lastIndexOf("/");
		if (index != -1)
			imageFileName = imageFileName.substring(index+1);

		// TODO: Proper...
		String str = String.format("<gendoc:pic imageId=\"%s\" keepH=\"%b\" keepW=\"%b\" maxH=\"%b\" maxW=\"%b\"/>", 
				imageId, keepH, keepW, maxH, maxW);
				
		return str;
	}
	
	public void replaceParagraphWithPicture(PPTXDocument document, XMLParser slideParser, Node slideParEl, String imageId, boolean keepH, boolean keepW, boolean maxH, boolean maxW) {
		try {
			Node slideShape = XPathPptxUtils.INSTANCE.evaluateNode(slideParEl, "ancestor::p:sp");		
			
			String nvPrId = XPathPptxUtils.INSTANCE.evaluateText(slideShape,"./p:nvSpPr/p:cNvPr/@id");
			String nvPrName = XPathPptxUtils.INSTANCE.evaluateText(slideShape,"./p:nvSpPr/p:cNvPr/@name");
			
			StringBuffer picBuffer = new StringBuffer();
			picBuffer.append("<p:pic>");
			picBuffer.append("<p:nvPicPr>");
			picBuffer.append("<p:cNvPr id=\""+nvPrId+"\" name=\""+nvPrName+"\"/>");
			picBuffer.append("<p:cNvPicPr>");
			picBuffer.append("<a:picLocks noGrp=\"1\" noChangeAspect=\"1\"/>");
			picBuffer.append("</p:cNvPicPr>");
			picBuffer.append("<p:nvPr/>");
			picBuffer.append("</p:nvPicPr>");
			picBuffer.append("<p:blipFill>");
			picBuffer.append("<a:blip r:embed=\""+imageId+"\">");
			picBuffer.append("</a:blip>");
			picBuffer.append("<a:stretch>");
			picBuffer.append("<a:fillRect/>");
			picBuffer.append("</a:stretch>");
			picBuffer.append("</p:blipFill>");
			picBuffer.append("<p:spPr/>");			
			picBuffer.append("</p:pic>");
			
			Node picNode = XPathPptxUtils.INSTANCE.parserXmlFragment(picBuffer.toString());
			picNode = slideShape.getParentNode().insertBefore(slideShape.getOwnerDocument().importNode(picNode,true), slideShape);			
						
			Node phNode = XPathPptxUtils.INSTANCE.evaluateNode(slideShape,"./p:nvSpPr/p:nvPr/p:ph");
			Element xfrm = (Element)XPathPptxUtils.INSTANCE.evaluateNode(slideShape,"./p:spPr/a:xfrm");
			if (xfrm == null && phNode != null) {
				String idx = XPathPptxUtils.INSTANCE.evaluateText(phNode,"./@idx");
				String type = XPathPptxUtils.INSTANCE.evaluateText(phNode,"./@type");
				String relsPath = document.getRelativePath(slideParser);
				relsPath = relsPath.replace("/slides/","/slides/_rels/")+".rels";
				XMLParser slideRelsPath = document.getSubdocument(relsPath);
				String layoutFile = XPathPptxUtils.INSTANCE.evaluateText(slideRelsPath.getDocument(),
						"//rel:Relationship[@Type='http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout']/@Target");
				layoutFile = "/ppt/slideLayouts/"+layoutFile.substring(layoutFile.lastIndexOf('/')+1);
				XMLParser slideLayoutParser = document.getSubdocument(layoutFile);
				
				xfrm = (Element)XPathPptxUtils.INSTANCE.evaluateNode(slideLayoutParser.getDocument(),"//p:sp[p:nvSpPr/p:nvPr/p:ph[@idx='"+idx+"']]/p:spPr/a:xfrm");
				if (xfrm == null)
					xfrm = (Element)XPathPptxUtils.INSTANCE.evaluateNode(slideLayoutParser.getDocument(),"//p:sp[p:nvSpPr/p:nvPr/p:ph[@type='"+type+"']]/p:spPr/a:xfrm");					

			}
			
			if (phNode != null) {
				Node pic_nvPrNode = XPathPptxUtils.INSTANCE.evaluateNode(picNode,"./p:nvPicPr/p:nvPr");
				pic_nvPrNode.appendChild(phNode.cloneNode(true));				
			}
			
			if (xfrm != null) {				
				// TODO: Change Size.
				long x = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm,"./a:off/@x",0);
				long y = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm,"./a:off/@y",0);
				long cx = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm,"./a:ext/@cx",0);
				long cy = XPathPptxUtils.INSTANCE.evaluateNumber(xfrm,"./a:ext/@cy",0);
				IDocumentService docServ = GendocServices.getDefault().getService(IDocumentService.class);
				ImageDimension imDim = resizeImage(
						document.getUnzipLocationDocumentFile()+File.separator+
							docServ.getAdditionalResourceService().getImageRelativePath(imageId).replace(
							'/', File.separatorChar), 
						(long)UnitsHelper.convertToPixels(Unit.EMU,cx,96),
						(long)UnitsHelper.convertToPixels(Unit.EMU,cy,96),
						keepH, keepW, maxH, maxW);
				
				// TODO: Query the current DPI.
				
				long newCx = (long)UnitsHelper.convertFromPixels(imDim.getWidth(),96,UnitsHelper.Unit.EMU);
				long newCy = (long)UnitsHelper.convertFromPixels(imDim.getHeight(),96,UnitsHelper.Unit.EMU);
				
				x += (cx - newCx) / 2;
				y += (cy - newCy) / 2;
				cx = newCx;
				cy = newCy;
				
				Element spPrNode = (Element)XPathPptxUtils.INSTANCE.evaluateNode(picNode, "./p:spPr");
				xfrm = (Element)spPrNode.appendChild(picNode.getOwnerDocument().createElementNS(
						PPTXHelper.DRAWING_ML_NAMESPACE, "a:xfrm")); 
				Element offNode = (Element)xfrm.appendChild(picNode.getOwnerDocument().createElementNS(
						PPTXHelper.DRAWING_ML_NAMESPACE, "a:off"));
				offNode.setAttribute("x", ""+x);
				offNode.setAttribute("y", ""+y);
				Element extNode = (Element)xfrm.appendChild(picNode.getOwnerDocument().createElementNS(
						PPTXHelper.DRAWING_ML_NAMESPACE, "a:ext"));
				extNode.setAttribute("cx", ""+cx);
				extNode.setAttribute("cy", ""+cy);
				Element prstGeom = (Element)spPrNode.appendChild(picNode.getOwnerDocument().createElementNS(
						PPTXHelper.DRAWING_ML_NAMESPACE, "a:prstGeom"));
				prstGeom.setAttribute("prst", "rect");
			}
		} catch (XPathExpressionException e) {
			Activator.log(e, IStatus.ERROR);
		} catch (Exception e) {
			Activator.log(e, IStatus.ERROR);
		}

	}	
}
