/*****************************************************************************
 * Copyright (c) 2010 Atos Origin.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Kris Robertson (Atos Origin) kris.robertson@atosorigin.com - Initial API and implementation
 * Antonio Campesino Robles (Ericsson) - Bug 478883
 * 
 *****************************************************************************/
package org.eclipse.gendoc.tags.handlers.impl.post;

import java.util.LinkedList;
import java.util.regex.Pattern;

import org.eclipse.gendoc.document.parser.documents.Document;
import org.eclipse.gendoc.document.parser.documents.Document.PROPERTY;
import org.eclipse.gendoc.document.parser.documents.helper.XMLHelper;
import org.eclipse.gendoc.documents.IDocumentService;
import org.eclipse.gendoc.process.AbstractStepProcess;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.GenDocException;
import org.eclipse.gendoc.tags.handlers.impl.RegisteredTags;
import org.w3c.dom.Node;

public class DropTagProcess extends AbstractStepProcess
{
	protected static final Pattern DROP_TAG_PATTERN = Pattern.compile("<\\s*" + RegisteredTags.DROP + "\\s*/\\s*>", Pattern.MULTILINE);
	
	protected IDocumentService documentService = GendocServices.getDefault().getService(IDocumentService.class);
	
	protected LinkedList<Node> nodesToRemove;

	@Override
	protected void doRun() throws GenDocException {
		nodesToRemove = new LinkedList<Node>();

		super.doRun();

		for (Node n : nodesToRemove) {
			Node parent = n.getParentNode();
			if (parent != null) {
				parent.removeChild(n);
			}
		}
	}

	@Override
	protected void step(Document document) throws GenDocException
	{
		Node currentNode = document.getXMLParser().getCurrentNode();
		if (documentService.isPara(currentNode.getNodeName())) {
			String text = (String)document.get(PROPERTY.text);
			if (text != null && DROP_TAG_PATTERN.matcher(text).find()) {
				Node parent = currentNode.getParentNode();
				if (parent != null && documentService.isCell(parent.getNodeName()) && !hasSiblingParas(currentNode)) {
					// Avoid remove the only paragraph inside the cell. We just clean it up. 
					cleanTextNodes(document, currentNode);
					document.getXMLParser().setCurrentNode(currentNode);
					return;
				}
				nodesToRemove.add(currentNode);
			}
		}
	}
	
	private boolean hasSiblingParas(Node n) {
		Node sib = n.getPreviousSibling();
		while (sib != null) {
			if (documentService.isPara(sib.getNodeName()))
				return true;
			sib = sib.getPreviousSibling();
		}
		
		sib = n.getNextSibling();
		while (sib != null) {
			if (documentService.isPara(sib.getNodeName()))
				return true;
			sib = sib.getNextSibling();
		}
		return false;
	}

	private void cleanTextNodes(Document doc, Node para) {
		String text = (String)doc.get(PROPERTY.text);
		if (!doc.next())
			return;
		Node n = doc.getXMLParser().getCurrentNode();
 
		while (XMLHelper.isAncestor(n, para)) {
			String value = n.getNodeValue();
			if (value != null && text.startsWith(value)) {
				text = text.substring(value.length());
				n.setNodeValue("");
			}
			if (!doc.next())
				return;
			n = doc.getXMLParser().getCurrentNode();
		}
		
	}
	
}
