/*****************************************************************************
 * Copyright (c) 2018 Ericsson AB.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.bundle.acceleo.html.files;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public interface IHtmlVisitor {
	public boolean visitDocument(Document node);
	public void visitDocumentAfter(Document node);
	public boolean visitElement(Element node);
	public void visitElementAfter(Element node);
	public void visitText(Text node);
}
