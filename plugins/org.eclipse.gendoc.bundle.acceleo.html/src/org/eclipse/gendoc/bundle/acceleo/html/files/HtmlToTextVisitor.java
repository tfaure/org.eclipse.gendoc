/*****************************************************************************
 * Copyright (c) 2018 Ericsson AB.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.bundle.acceleo.html.files;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class HtmlToTextVisitor extends HtmlStripTagVisitor implements IHtmlVisitor {
	@Override
	public boolean visitElement(Element node) {
		boolean res = super.visitElement(node);
		String tag = node.getTagName();
		if (tag.equals("ol")) {
			listIndent++;
			lists.add(0);
		} else if(tag.equals("ul")) {
			listIndent++;
			lists.add(-1);
		} else if (tag.equals("li")) {
			item = true;
			if (lists.size() == 0)
				lists.add(0);
			int l = lists.get(lists.size()-1);
			if (l >= 0) {
				lists.set(lists.size()-1,l+1);
			}
		}
		return res;
	}

	@Override
	public void visitElementAfter(Element node) {
		String tag = node.getTagName();
		if (tag.equals("ol")) {
			listIndent--;
			lists.remove(lists.size()-1);
		} else if(tag.equals("ul")) {
			listIndent--;
			lists.remove(lists.size()-1);
		}
		super.visitElementAfter(node);
	}

	@Override
	public void visitText(Text node) {
		if (newLine) {
			for (int i=0; i<listIndent; i++) {
				buf.append("\t");
			}			
		}
		newLine = false;
		
		if (item && lists.size()>0) {
			int l = lists.get(lists.size()-1);
			if (l < 0) {
				buf.append("- ");
			} else if (l >= 0) {
				buf.append(l).append(". ");
			}
		}
		item = false;
		super.visitText(node);
	}
	
	@Override
	protected void newLine() {
		super.newLine();
		newLine = true;
	}

	private int listIndent = 0;
	private boolean newLine = false;
	private boolean item = false;
	private List<Integer> lists = new ArrayList<Integer>();
}
