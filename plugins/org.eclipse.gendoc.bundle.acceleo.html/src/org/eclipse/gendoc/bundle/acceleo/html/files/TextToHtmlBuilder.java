/*****************************************************************************
 * Copyright (c) 2018 Ericsson AB.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.bundle.acceleo.html.files;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.gendoc.bundle.acceleo.html.Activator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TextToHtmlBuilder {
	private static final Transformer TRANSFORMER = initTransformer();
	private static Transformer initTransformer() {
		try {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			tr.setOutputProperty(OutputKeys.INDENT, "true");
			tr.setOutputProperty(OutputKeys.METHOD, "html");
			tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			tr.setOutputProperty(OutputKeys.STANDALONE, "no");
			return tr;
		} catch (TransformerConfigurationException e) {
			Activator.error(e.getMessage(), e);
			return null;
		}
	}

	private static final DocumentBuilder DOC_BUILDER = initDocBuilder();
	private static DocumentBuilder initDocBuilder() {
		try {
			return DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			Activator.error(e.getMessage(), e);
			return null;
		}
	}
	
	private static final NumberFormat FLOAT_FORMAT = DecimalFormat.getNumberInstance(Locale.US);
	private static final Pattern PATTERN_ROMAN = Pattern.compile(
			"(M|MM|MMM|m|mm|mmm)?(C|CC|CCC|CD|D|DC|DCC|DCCC|CM|c|cc|ccc|cd|d|dc|dcc|dccc|cm)?"+
			"(X|XX|XXX|XL|L|LX|LXX|LXXX|XC|x|xx|xxx|xl|l|lx|lxx|lxxx|xc)?"+
			"(I|II|III|IV|V|VI|VII|VIII|IX|i|ii|iii|iv|v|vi|vii|viii|ix)?"+
			"(\\.|\\)|\\.-|\\-.)\\s+");
	private static final Pattern PATTERN_ORDER = Pattern.compile("([0-9]+|[a-zA-Z])(\\.([0-9]+|[a-zA-Z]))*(\\.|\\)|\\.-|\\-.|\\))\\s+");
	private static final Pattern PATTERN_SYM = Pattern.compile("[\\-\\+\\*\\¤\\>o]\\s+");
	private static final Pattern[] PATTERNS = new Pattern[] {
			PATTERN_SYM, PATTERN_ORDER, PATTERN_ROMAN};
	private static final Pattern[] PATTERNS_ORDER = new Pattern[] {
			PATTERN_ORDER, PATTERN_ROMAN};

	public TextToHtmlBuilder() {
		document = DOC_BUILDER.newDocument();
		element = (Element)document.appendChild(document.createElement("html"));
	}
	
	public void addText(String str) {
		try {
			BufferedReader r = new BufferedReader(new StringReader(str));
			String line = r.readLine();
			while (line != null) {
				int indent = getIndent(line,tabSpaces);
				int spaces = getWhitespaces(line);			
				line = line.substring(spaces);
				String listPrefix = getListPrefix(line);
				if (listPrefix != null) {				
					line = line.substring(listPrefix.length()); 
					//line = replaceTabs("&emsp;").replaceMultipleSpaces("&nbsp;");
					listItem(indent,listPrefix, line);
				} else {				
					//line = replaceTabs("&emsp;").replaceMultipleSpaces("&nbsp;");
					paragraph(indent, line);
				}
				line = r.readLine();
			}
		} catch (Exception e) {
			Activator.error(e.getMessage(), e);
		}			
	}

	protected void listItem(int indent, String prefix, String line) {
		boolean isNumber = isNumber(prefix);
		int diff = indent - this.indentation;
		
		if (diff > 0) {
			indentation = indent;
			listTabs.push(indentation);
			element = (Element)element.appendChild(document.createElement(isNumber?"ol":"ul"));				
		} else if (diff < 0) {
			popIndentation(indent);
		}
		if (element.getTagName().equals("li"))
			element = (Element)element.getParentNode();
		
		element = (Element)element.appendChild(document.createElement("li"));
		element.appendChild(document.createTextNode(line));
	}
	
	protected void paragraph(int indent, String line) {
		int diff = indent - this.indentation;
		
		if (this.indentation == -1) {
			// Add Indentation style.
			Element pEl = (Element)element.appendChild(document.createElement("p"));
			pEl.appendChild(document.createTextNode(line));
			diff--;
			if (diff > 0)
				pEl.setAttribute("style", "margin-left:"+ FLOAT_FORMAT.format(tabSize*diff)+"cm;");
			return;
		}
		
		if (diff > 0) {
			Element pEl = (Element)element.appendChild(document.createElement("p"));
			pEl.appendChild(document.createTextNode(line));
			if (diff > 0)
				pEl.setAttribute("style", "margin-left:"+ FLOAT_FORMAT.format(tabSize*diff)+"cm;");
			return;
		} else if (diff < 0 || (indentation == 0 && diff == 0)) {
			popIndentation(indent);
		}		
		
		if (element.getTagName().equals("li")) {
			element.appendChild(document.createElement("br"));
			element.appendChild(document.createTextNode(line));
		} else {
			element.appendChild(document.createElement("p")).appendChild(document.createTextNode(line));			
		}
	}

	private void popIndentation(int indent) {
		while (indentation > indent) {
			if (element.getTagName().equals("li")) {
				element = (Element)element.getParentNode();
			}
			// Closing <ol> or <ul>
			element = (Element)element.getParentNode();
			if(listTabs.size() <= 1) {
				indentation = -1;
			} else {
				listTabs.pop();
				indentation = listTabs.peek(); 			
			}			
		}		
	}
	
	private int getIndent(String line, int tab) {
		int nInd = 0;
		for (int i=0; i<line.length(); i++) {
			char ch = line.charAt(i);
			if (ch == '\t') {
				nInd+=tab;
			} else if (Character.isWhitespace(ch)){
				nInd++;
			} else {
				return nInd;
			}
		}
		return nInd;
	}

	private int getWhitespaces(String line) {
		int nInd = 0;
		for (int i=0; i<line.length(); i++) {
			char ch = line.charAt(i);
			if (Character.isWhitespace(ch)){
				nInd++;
			} else {
				return nInd;
			}
		}
		return nInd;
	}

	private String getListPrefix(String line) {
		for (Pattern p : PATTERNS) {
			Matcher m = p.matcher(line);
			if (m.find() && m.start() == 0) {
				String prefix = line.substring(0,m.end());
				if (!prefix.isEmpty())
					return prefix;
			} 
		}
		return null;
	}
	
	private boolean isNumber(String line) {
		for (Pattern p : PATTERNS_ORDER) {
			Matcher m = p.matcher(line);
			if (m.matches()) {
				return true;
			} 
		}
		return false;
	}

	@Override
	public String toString() {
		StringWriter w = new StringWriter();
		try {
			NodeList nl = document.getDocumentElement().getChildNodes();
			for (int i=0; i<nl.getLength(); i++) {
				TRANSFORMER.transform(new DOMSource(nl.item(i)), new StreamResult(w));
				w.append("\n");
			}
		} catch (TransformerException e) {
			Activator.error(e.getMessage(), e);
		}
		return w.toString();
	}
	
	private Document document;
	private Element element;
	private int tabSpaces = 4;
	private float tabSize = .25f;
	private Stack<Integer> listTabs = new Stack<Integer>();
	private int indentation = -1;
}
