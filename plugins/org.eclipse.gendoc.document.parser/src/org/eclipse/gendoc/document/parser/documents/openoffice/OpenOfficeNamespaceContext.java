/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.documents.openoffice;

import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

public class OpenOfficeNamespaceContext implements NamespaceContext
{
	private final String[] mapping;
	private final String prefixMapping; 

	public OpenOfficeNamespaceContext(String[] mapping) {
		this.mapping = mapping;
		StringBuffer buf = new StringBuffer();
		buf.append("xmlns=\"").append(mapping[1]).append("\" ");
		
		for (int i=2; i<mapping.length; i+=2) {
			buf.append("xmlns:").append(mapping[i]).append("=\"").append(mapping[i+1]).append("\" ");
		}
		
		prefixMapping = buf.toString().trim();
	}
	
	public final String getPrefixMapping() {
		return prefixMapping;
	}
	
    public String getNamespaceURI(String prefix)
    {
    	if (prefix == null || prefix.equals(""))
        {
            return mapping[1]; 
        }

    	for (int i=2; i<mapping.length; i+=2) {
			if (prefix.equals(mapping[i]))
				return mapping[i+1];
		}

    	return XMLConstants.NULL_NS_URI;

    }

    public String getPrefix(String uri)
    {
    	for (int i=0; i<mapping.length; i+=2) {
			if (uri.equals(mapping[i+1]))
				return mapping[i];
		}
    	return null;
    }

    @SuppressWarnings("rawtypes")
	public Iterator getPrefixes(String uri)
    {
    	return new Iterator<String>() {
			@Override
			public boolean hasNext() {
				return pos < mapping.length;
			}

			@Override
			public String next() {
				String str = mapping[pos];
				pos += 2;
				return str;
			}
			
			private int pos = 0; 
		};
    }

}
