/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.pptx;

import java.io.File;

import org.eclipse.gendoc.document.parser.documents.Document;
import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeParser;

public class PPTXParser extends OpenOfficeParser {
	public PPTXParser(PPTXDocument document, File f, Document.CONFIGURATION idForDocument) {
		super(document, f, idForDocument);
	}

	@Override
	protected XPathPptxUtils getXPathUtils() {
		return XPathPptxUtils.INSTANCE;
	}
	
	@Override
	protected void parse() {
		super.parse();
	}

}

