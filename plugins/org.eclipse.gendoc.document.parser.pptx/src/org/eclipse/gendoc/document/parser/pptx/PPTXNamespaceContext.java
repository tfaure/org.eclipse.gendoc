/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.pptx;

import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeHelper;
import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeNamespaceContext;

public class PPTXNamespaceContext extends OpenOfficeNamespaceContext {
	private static final String[] MAPPING = new String[] {
			"", PPTXHelper.PRESENTATION_NAMESPACE,
			"p", PPTXHelper.PRESENTATION_NAMESPACE,
			"mc", OpenOfficeHelper.MARKUP_COMPATIBILITY_NAMESPACE,		
			"a", OpenOfficeHelper.DRAWING_ML_NAMESPACE,
			"ct", OpenOfficeHelper.CONTENT_TYPES_NAMESPACE, 
			"rel", OpenOfficeHelper.PACKAGE_RELATIONSHIPS_NAMESPACE,
			"r", OpenOfficeHelper.RELATIONSHIPS_NAMESPACE,
			"gendoc", OpenOfficeHelper.GENDOC_NS
	};
	
	public PPTXNamespaceContext() {
		super(MAPPING);
	}

}
