/*****************************************************************************
 * (c) Copyright 2016 Telefonaktiebolaget LM Ericsson
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Antonio Campesino (Ericsson) antonio.campesino.robles@ericsson.com - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.gendoc.document.parser.pptx;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.gendoc.document.parser.documents.DefaultXMLParserProvider;
import org.eclipse.gendoc.document.parser.documents.IXMLParserProvider;
import org.eclipse.gendoc.document.parser.documents.XMLParser;
import org.eclipse.gendoc.document.parser.documents.Document.CONFIGURATION;
import org.eclipse.gendoc.document.parser.documents.helper.OfficeHelper;
import org.eclipse.gendoc.document.parser.documents.helper.XMLHelper;
import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeDocument;
import org.w3c.dom.Node;

/**
 * The Class PPTXDocument.
 */
public class PPTXDocument extends OpenOfficeDocument
{
    public PPTXDocument(File documentFile) throws IOException
    {
        this(documentFile.toURI().toURL(), null);
    }

    public PPTXDocument(File documentFile, Map<CONFIGURATION, Boolean> configuration) throws IOException
    {
        this(documentFile.toURI().toURL(), configuration);

    }

    public PPTXDocument(URL documentFile, Map<CONFIGURATION, Boolean> configuration)
    {
        super(documentFile,configuration);
    }

    @Override
    protected Collection<XMLParser> getXmlParsers(CONFIGURATION idForDocument)
    {
        Collection<XMLParser> parsers = new LinkedList<XMLParser>();
        
        switch (idForDocument)
        {
            case content:
            	XMLParser presentation = new PPTXParser(this, getUnzipper().getFile(PPTXHelper.PRESENTATION_FILE_NAME), idForDocument);
            	parsers.add(presentation);
                break;
            case footer:
                break;
            case header:
                break;
            case comment:
                // TODO
            default:
        }
        
        return parsers;
    }

	@Override
	public String getTextCorrespondingToCurrentStyle() {
		return null;
	}

	public String getText() {
        StringBuffer result = new StringBuffer("");
        Node n = getXMLParser().getCurrentNode();
        if (PPTXHelper.DRAWING_ML_TEXT_PARAGRAPH.equals(n.getNodeName()))
        {
            Node tmp = XMLHelper.next(n);
            while (tmp != null && !PPTXHelper.DRAWING_ML_TEXT_PARAGRAPH.equals(tmp.getNodeName()))
            {
                if (PPTXHelper.DRAWING_ML_TEXT_STRING.equals(tmp.getNodeName()))
                {
                    result.append(tmp.getTextContent());
                }
                tmp = XMLHelper.next(tmp);
            }
        }
        return result.toString();
	}
	
    public Object get(PROPERTY property)
    {
        switch (property)
        {
            case text:
                return getText();
            default:
                return null;
        }
    }
}
