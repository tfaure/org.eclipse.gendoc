/*****************************************************************************
 * Copyright (c) 2010 Atos Origin.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Caroline Bourdeu d'Aguerre (Atos Origin) caroline.bourdeudaguerre@atosorigin.com - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.gendoc.tags.parsers;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * The Interface TagParserConfig.
 * 
 * @author cbourdeu
 */
public interface TagParserConfig
{

    /** The INF. */
    String INF = "&lt;";

    /** The SUP. */
    String SUP = "&gt;";

    /** The SLASH. */
    String SLASH = "/";
    
    char BACKSLASH = '\u005C\';
    char SLASH_CHAR = '\u002F';

    Set<Character> INVALID_SPACES = Collections.unmodifiableSet(
    		new HashSet<Character>(Arrays.asList('\u00A0','\u2000','\u2000','\u2001',
    				'\u2002','\u2003','\u2004','\u2005','\u2006','\u2007','\u2008','\u2009',
    				'\u200A','\u200B','\u202F','\u205F','\u3000','\uFEFF')));

    /** The SPACE. */
    char SPACE = '\u0020';

    Set<Character> INVALID_QUOTES = Collections.unmodifiableSet(
    		new HashSet<Character>(Arrays.asList('\u2018','\u2019','\u0060','\u00B4')));

    /** The QUOTE. */
    char VALID_QUOTE = "\u0027".charAt(0);

    
    Set<Character> INVALID_DOUBLE_QUOTES = Collections.unmodifiableSet(
    		new HashSet<Character>(Arrays.asList('\u201C','\u201D')));
    char VALID_DOUBLE_QUOTE = '\u0022';
    
    /** The EQUAL. */
    String EQUAL = "=";

    /** The EMPTY. */
    String EMPTY = "";

    /** The XM l_ quote. */
    String XML_QUOTE = "�";

    // String QUOTE = "'";
    // String INF = "&lt;";
    // String SUP = "&gt;";
    // String SLASH = "/";
    // String SPACE = " ";

}
