/*****************************************************************************
 * Copyright (c) 2017 Atos.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Mohamed Ali Bach Tobji (Atos) mohamed-ali.bachtobji@atos.net - - Initial API and implementation
 * Anne Haugommard (Atos for PlasticOmnium) - Fix bug #535660 - table cell containing special characters
 *****************************************************************************/

package org.eclipse.gendoc.services.odt;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gendoc.bundle.acceleo.commons.files.CommonService;
import org.eclipse.gendoc.documents.AbstractTableService;
import org.eclipse.gendoc.documents.IDocumentService;
import org.eclipse.gendoc.services.exception.InvalidContentException;
import org.eclipse.gendoc.tags.ITag;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.eclipse.gendoc.table.Cell;
import org.eclipse.gendoc.table.Row;
import org.eclipse.gendoc.table.Table;

public class ODTTableService extends AbstractTableService {

	@Override
	public String manageTable(ITag tag, IDocumentService documentService, StringBuffer returnValue, Object tableModel,
			Node tableNode) throws InvalidContentException {

		Node mainTable = tableNode.getFirstChild();
		if (documentService.isTable(mainTable.getNodeName())) {
			if (tableModel instanceof Table) {

				int nbCol = ((Table) tableModel).getColCount();
				String colLabel = "table:table-column";
				int index_col_start = returnValue.indexOf("<" + colLabel);
				returnValue.insert(index_col_start + colLabel.length() + 1,
						" table:number-columns-repeated=\"" + nbCol + "\"");
				Node rowNode = getChildNode(documentService.getRowLabel(), mainTable);
				String rowLabel = documentService.getRowLabel();

				Pattern rowPattern = Pattern.compile("<" + rowLabel + "[^<>]*>.*</" + rowLabel + ">");
				Matcher rowm = rowPattern.matcher(returnValue);
				int index_row_start = 0;
				int index_row_end = 0;
				if (rowm.find()) {
					index_row_start = rowm.start();
					index_row_end = returnValue.lastIndexOf("</" + rowLabel + ">");

					String row = returnValue.substring(index_row_start, index_row_end + rowLabel.length() + 3);

					String cellLabel = documentService.getCellLabel();
					int index_cell_start = row.indexOf("<" + cellLabel);
					int index_cell_end = row.lastIndexOf("</" + cellLabel + ">");

					Node cellNode = getChildNode(documentService.getCellLabel(), rowNode);
					if (cellNode != null) {
						StringBuffer colNodetxt = new StringBuffer(documentService.asText(cellNode));
						String txtLabel = "text:p";
						Pattern txtPattern = Pattern.compile("<" + txtLabel + "[^<>]*>.*</" + txtLabel + ">");
						Matcher txtm = txtPattern.matcher(colNodetxt);
						if (txtm.find()) {
							String style = "";
							return generateTable(returnValue, tableModel, rowLabel, index_row_start, index_row_end, row,
									cellLabel, index_cell_start, index_cell_end, colNodetxt, txtm, style, txtLabel);
						}
					}
				}
			}

		}
		return null;
	}

	private Node getChildNode(String label, Node node) {

		NodeList rowChildNodes = node.getChildNodes();
		for (int i = 0; i < rowChildNodes.getLength(); i++) {
			Node current = rowChildNodes.item(i);
			if (current.getNodeName() == label) {
				return current;
			}
		}
		return null;
	}

	/**
	 * @param returnValue
	 * @param tableModel
	 * @param rowLabel
	 * @param index_row_start
	 * @param index_row_end
	 * @param row
	 * @param colLabel
	 * @param index_col_start
	 * @param index_col_end
	 * @param colNodetxt
	 * @param txtm
	 * @param rows
	 * @param style
	 * @param txtLabel
	 * @return
	 */
	private String generateTable(StringBuffer returnValue, Object tableModel, String rowLabel, int index_row_start,
			int index_row_end, String row, String colLabel, int index_col_start, int index_col_end,
			StringBuffer colNodetxt, Matcher txtm, String style, String txtLabel) {
		String rows = "";
		String cells = "";
		// generating table header
		StringBuffer newRow = new StringBuffer(row);
		cells = generateCells(((Table) tableModel).getTableheader().getCells(), colNodetxt, txtm, style, txtLabel);
		newRow.replace(index_col_start, index_col_end + colLabel.length() + 3, cells);
		rows += newRow.toString();
		// generating table body
		for (Row r : ((Table) tableModel).getRows()) {
			newRow = new StringBuffer(row);
			cells = generateCells(r.getCells(), colNodetxt, txtm, style, txtLabel);
			newRow.replace(index_col_start, index_col_end + colLabel.length() + 3, cells);
			rows += newRow.toString();
		}
		return returnValue.replace(index_row_start, index_row_end + rowLabel.length() + 3, rows).toString();
	}

	/**
	 * @param modelCells
	 * @param colNodetxt
	 * @param txtm
	 * @param style
	 * @param txtLabel
	 * @param cols
	 * @return
	 */
	private String generateCells(EList<Cell> modelCells, StringBuffer colNodetxt, Matcher txtm, String style,
			String txtLabel) {
		String cols = "";
		for (Cell cell : modelCells) {
			StringBuffer newCol;
			int txtindex = txtm.end() - txtLabel.length() - 3;
			newCol = new StringBuffer(colNodetxt.toString());
			// Bug 535660 : Table celle containing special characters corrupts the generated document
			newCol.insert(txtindex, CommonService.cleanAndFormat(cell.getLabel()));
			cols += newCol.toString();
		}
		return cols;
	}

}
