/*****************************************************************************
 * Copyright (c) 2011 Atos
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * Tristan FAURE (Atos) tristan.faure@atos.net - Initial API and implementation
 *****************************************************************************/
package org.eclipse.gendoc.script.acceleo.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gendoc.m2t.IFragmentService;
import org.eclipse.gendoc.m2t.model.Argument;
import org.eclipse.gendoc.m2t.model.Fragment;
import org.eclipse.gendoc.script.acceleo.ServicesExtension;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.exception.GenDocException;
import org.eclipse.gendoc.tags.handlers.IContextService;

/**
 * A script generator specific to fragments
 * 
 * @author tfaure
 * s
 */
public class FragmentScriptGenerator extends ScriptGenerator
{

    private final List<Fragment> fragments;
    private Fragment currentFragment;

    public FragmentScriptGenerator(List<Pattern> patterns, List<Fragment> fragments)
    {
        super(patterns);
        this.fragments = fragments;
    }

    public String formatScript(EObject element, String s) throws GenDocException
    {
        IContextService contextService = GendocServices.getDefault().getService(IContextService.class);

        StringBuilder resultScript = new StringBuilder();
        insertModule(element, contextService, resultScript);
        addImports(element, resultScript);

        StringBuilder queryScripts = new StringBuilder();
        StringBuilder templateScripts = new StringBuilder();

        for (Fragment f : fragments) {
        	currentFragment = f;
	        final Pattern pattern = getQueryPattern();
	        String script = f.getScriptValue();
	        List<String> queries = extractQueries(script, pattern);
	        String scriptWithoutQueries = cleanFromQueries(script, pattern);
	        for (String query : queries)
	        {
	            queryScripts.append(query + "\n");
	        }
	        
	        addTemplate(element, templateScripts, scriptWithoutQueries);
        }
        
        resultScript.append(queryScripts);
        resultScript.append(templateScripts);
        
        return resultScript.toString();
    }


    @Override
    protected void addImports(EObject element, StringBuilder resultScript) throws GenDocException
    {
    	Set<String> bundles = new HashSet<String>();
    	bundles.addAll(ServicesExtension.getInstance().getBundlesImportedByDefault());
        for (Fragment f : fragments)
        {
        	bundles.addAll(f.getImportedBundles());
            //bundles.addAll(f.getImportedFragments());
        }
        
        for (String bundle : bundles) {
        	super.addImport(element, resultScript, bundle);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.gendoc.script.acceleo.impl.ScriptGenerator#addModuleImport()
     * 
     * @override
     */
    protected boolean addModuleImport()
    {
        // a fragment can import other fragments
        return true;
    }

    @Override
    protected void appendModuleName(StringBuilder resultScript)
    {
        resultScript.append(IFragmentService.FRAGMENT_MODULE_NAME);
    }

    @Override
    protected String getTemplateName()
    {
        return currentFragment.getName();
    }

    @Override
    protected boolean generatesFile()
    {
        return false;
    }

    @Override
    protected String getTemplateArguments(EObject element)
    {
        StringBuffer result = new StringBuffer();
        boolean flag = false;
        for (Argument a : currentFragment.getArguments())
        {
            if (flag)
            {
                result.append(", ");
            }
            result.append(a.getName()).append(" : ").append(a.getType());
            flag = true;
        }
        return result.toString();
    }

    @Override
    protected String getTemplateVisibility()
    {
        // no matter ScriptGenerator is modified this template must be public
        return "public";
    }
}
