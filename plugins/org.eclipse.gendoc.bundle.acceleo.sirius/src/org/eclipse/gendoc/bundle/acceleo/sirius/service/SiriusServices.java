package org.eclipse.gendoc.bundle.acceleo.sirius.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.gendoc.bundle.acceleo.commons.files.CommonService;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.sirius.business.api.session.CustomDataConstants;
import org.eclipse.sirius.diagram.DSemanticDiagram;
import org.eclipse.sirius.table.metamodel.table.DTable;
import org.eclipse.sirius.table.tools.internal.export.csv.TableCsvHelper;
import org.eclipse.sirius.viewpoint.description.AnnotationEntry;

import org.eclipse.gendoc.table.Cell;
import org.eclipse.gendoc.table.Row;
import org.eclipse.gendoc.table.Table;
import org.eclipse.gendoc.table.TableFactory;
import org.eclipse.gendoc.table.TableHeader;


public class SiriusServices {
	
	public List<Diagram> getSiriusDiagrams (EObject self){
		// TO IMPROVE ? reuse existing sirius services ?
		CommonService.load(self, "aird");
		ResourceSet set = null;
		if (self.eResource() != null && self.eResource().getResourceSet() != null){
			set = self.eResource().getResourceSet();
		}
		if (set == null){
			return Arrays.asList(); 
		}
		ECrossReferenceAdapter cross = ECrossReferenceAdapter.getCrossReferenceAdapter(set);
		if (cross == null){
			cross = new ECrossReferenceAdapter();
			set.eAdapters().add(cross);
		}
		List<Diagram> result = new LinkedList<Diagram>();
		Collection<Setting> inverses = cross.getInverseReferences(self, true);
		for (Setting s : inverses){
			if (s.getEObject() instanceof DSemanticDiagram) {
				DSemanticDiagram sem = (DSemanticDiagram) s.getEObject();
				for (AnnotationEntry e : sem.getOwnedAnnotationEntries()){
					if (CustomDataConstants.GMF_DIAGRAMS.equals(e.getSource()) && e.getData() instanceof Diagram){
						result.add((Diagram) e.getData());
					}
				}
				
			}
		}
		return result;
	}
	
	/**
	 * Get sirius tables under element object
	 * @param element
	 * @return
	 */
	public Collection<org.eclipse.gendoc.table.Table> getSiriusTables(EObject element) {
		// TO IMPROVE ? reuse existing sirius services ?
				CommonService.load(element, "aird");
				ResourceSet set = null;
				if (element.eResource() != null && element.eResource().getResourceSet() != null){
					set = element.eResource().getResourceSet();
				}
				if (set == null){
					return null; 
				}
				ECrossReferenceAdapter cross = ECrossReferenceAdapter.getCrossReferenceAdapter(set);
				if (cross == null){
					cross = new ECrossReferenceAdapter();
					set.eAdapters().add(cross);
				}
						
				Collection<Setting> inverses = cross.getInverseReferences(element, true);
		Collection<org.eclipse.gendoc.table.Table> results = new LinkedList<org.eclipse.gendoc.table.Table>();
		for (Setting s : inverses){
			if (s.getEObject() instanceof DTable) {
				
				DTable sem = (DTable) s.getEObject();
				Table result = TableFactory.eINSTANCE.createTable();
				result.setName(sem.getName());
				Iterable<Iterable<String>> tableDescriptors = TableCsvHelper.getTableDescriptor(sem);
				int index=0;
				for (Iterable<java.lang.String> line : tableDescriptors) {
					if (index==0){
						TableHeader header = TableFactory.eINSTANCE.createTableHeader();
						for (String cellValue : line) {
							Cell cell = TableFactory.eINSTANCE.createCell();
			                cell.setLabel(cellValue);
			                header.getCells().add(cell);
			            }
						result.setTableheader(header);
					} else {
					
					 Row row = TableFactory.eINSTANCE.createRow();
					 for (String cellValue : line) {
						Cell cell = TableFactory.eINSTANCE.createCell();
		                cell.setLabel(cellValue);
		                row.getCells().add(cell);
		             }
					 result.getRows().add(row);
					}
		            index++;
		        }
				results.add(result);
			}
		}
		return results;
	}
	
	static boolean isSiriusDiagram(Diagram diagram) {
		if (!(diagram.eContainer() instanceof AnnotationEntry))
			return false;
		
		AnnotationEntry e = (AnnotationEntry)diagram.eContainer();
		return CustomDataConstants.GMF_DIAGRAMS.equals(e.getSource()) && e.getData() == diagram;
	}
}
