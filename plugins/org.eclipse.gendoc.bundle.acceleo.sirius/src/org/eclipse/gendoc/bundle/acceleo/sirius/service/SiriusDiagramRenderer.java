package org.eclipse.gendoc.bundle.acceleo.sirius.service;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.RootEditPart;
import org.eclipse.gendoc.bundle.acceleo.gmf.service.GMFDiagramRenderer;
import org.eclipse.gmf.runtime.diagram.core.listener.DiagramEventBroker;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IDiagramPreferenceSupport;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramCommandStack;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditDomain;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramGraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.services.editpart.EditPartService;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.sirius.diagram.ui.tools.internal.part.SiriusDiagramGraphicalViewer;
import org.eclipse.swt.widgets.Shell;

public class SiriusDiagramRenderer extends GMFDiagramRenderer {
	public SiriusDiagramRenderer() {
		super();
	}

	@Override
	public DiagramEditPart createDiagramEditPart(Diagram diagram, Shell shell, PreferencesHint preferencesHint) {
		if (isSiriusDiagram(diagram)) {
			DiagramGraphicalViewer customViewer = new SiriusDiagramGraphicalViewer();
	        customViewer.createControl(shell);
	
	        DiagramEditDomain editDomain = new DiagramEditDomain(null);
	        editDomain.setCommandStack(
	            new DiagramCommandStack(editDomain));
	
	        customViewer.setEditDomain(editDomain);
	
	        // hook in preferences
	        RootEditPart rootEP = EditPartService.getInstance().createRootEditPart(
	            diagram);
	        if (rootEP instanceof IDiagramPreferenceSupport) {
	            if (preferencesHint == null) {
	                preferencesHint = ((IDiagramPreferenceSupport) rootEP)
	                    .getPreferencesHint();
	            } else {
	                ((IDiagramPreferenceSupport) rootEP)
	                    .setPreferencesHint(preferencesHint);
	            }
	            customViewer
	                .hookWorkspacePreferenceStore((IPreferenceStore) preferencesHint
	                    .getPreferenceStore());
	        }
	        
	        customViewer.setRootEditPart(rootEP);
	
	        customViewer.setEditPartFactory(EditPartService.getInstance());
	
	        DiagramEventBroker.startListening(TransactionUtil.getEditingDomain(diagram));
	        
	        customViewer.setContents(diagram);
	        customViewer.flush();
	        
	        Assert.isTrue(customViewer.getContents() instanceof DiagramEditPart);
	        
	    	/*
	    	 * We need to flush all the deferred updates. 
	    	 */
	   		while (shell.getDisplay().readAndDispatch()) {
	   			// nothing
	   		}
	        
	        return (DiagramEditPart) customViewer.getContents();
		}
		return super.createDiagramEditPart(diagram, shell, preferencesHint);
	}
	
	private boolean isSiriusDiagram(Diagram diagram) {
		return SiriusServices.isSiriusDiagram(diagram);
	}
}
