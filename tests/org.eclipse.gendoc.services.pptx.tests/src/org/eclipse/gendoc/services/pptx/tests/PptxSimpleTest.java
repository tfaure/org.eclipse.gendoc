package org.eclipse.gendoc.services.pptx.tests;

import java.net.URL;

import org.eclipse.gendoc.document.parser.pptx.PPTXNamespaceContext;
import org.eclipse.gendoc.document.parser.pptx.XPathPptxUtils;
import org.eclipse.gendoc.services.openoffice.test.OpenOfficeTestInputTemplate;
import org.eclipse.gendoc.services.openoffice.test.OpenOfficeTestSupport;
import org.eclipse.gendoc.services.openoffice.test.OpenOfficeVerifyHelper;
import org.junit.Test;

public class PptxSimpleTest {	
	@Test
	public void simpleTest() throws Exception {
		String testName = "simpleTest";
		OpenOfficeTestInputTemplate template = new OpenOfficeTestInputTemplate(
				"org.eclipse.gendoc.services.pptx",
				new URL("platform:/fragment/org.eclipse.gendoc.services.pptx.tests/resources/template.pptx"),
				XPathPptxUtils.INSTANCE);
		OpenOfficeVerifyHelper verifier = OpenOfficeTestSupport.execute(
				"org.eclipse.gendoc.services.pptx",
				testName, 
				template, 
				new PPTXNamespaceContext(),
				new URL("platform:/fragment/org.eclipse.gendoc.services.pptx.tests/"
						+ "resources/OpenOfficeSimpleTest.uml"), 
				new URL("platform:/meta/org.eclipse.gendoc.services.pptx/test/"+
						testName+".res.pptx"));
	}
	
}