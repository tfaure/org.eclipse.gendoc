package org.eclipse.gendoc.services.openoffice.test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.gendoc.GendocProcess;
import org.eclipse.gendoc.document.parser.documents.openoffice.OpenOfficeNamespaceContext;
import org.eclipse.gendoc.services.GendocServices;
import org.eclipse.gendoc.services.IGendocDiagnostician;
import org.eclipse.gendoc.services.IProgressMonitorService;
import org.eclipse.gendoc.services.exception.GenDocException;
import org.eclipse.gendoc.tags.handlers.IConfigurationService;
import org.junit.Assert;

public class OpenOfficeTestSupport {
	public static OpenOfficeVerifyHelper execute(String pluginId, String test, OpenOfficeTestInputTemplate input, 
			OpenOfficeNamespaceContext namespaceContext,  URL model, URL output) 
			throws IOException, URISyntaxException, GenDocException, TransformerException, ParserConfigurationException  {
		String fileExtension = input.getBaseTemplate().getFile();
		fileExtension = fileExtension.substring(fileExtension.lastIndexOf('.')+1);
		
		URL newInput = new URL("platform:/meta/"+pluginId+"/test/"+
				test+".input."+fileExtension);
	    File f = URIUtil.toFile(URIUtil.toURI(FileLocator.resolve(newInput)));
	    f.getParentFile().mkdirs();
	    input.generate(f);
		return execute(test, newInput, namespaceContext, model, output);
	}
	
	public static OpenOfficeVerifyHelper execute(String test, URL input, OpenOfficeNamespaceContext namespaceContext, 
			URL model, URL output) 
			throws IOException, URISyntaxException, GenDocException, ParserConfigurationException  {
		IGendocDiagnostician diagnostician = GendocServices.getDefault().getService(IGendocDiagnostician.class);
		diagnostician.init();
		IProgressMonitorService monitorService = (IProgressMonitorService) GendocServices.getDefault().getService(IProgressMonitorService.class);
		monitorService.setMonitor(new NullProgressMonitor());

		File f = URIUtil.toFile(URIUtil.toURI(FileLocator.resolve(output)));
		f.getParentFile().mkdirs();

		IConfigurationService parameter = GendocServices.getDefault().getService(IConfigurationService.class);
		parameter.addParameter("output", f.getAbsolutePath());
				
		URL resolvedUrl = FileLocator.resolve(model);
		if (resolvedUrl.getProtocol().equals("file")) {	
			String eclipseTarget = System.getenv("eclipse.target");
			if (eclipseTarget != null) {
				String fileStr = resolvedUrl.getFile();
				String ext = fileStr.substring(fileStr.lastIndexOf('.'));
				fileStr = fileStr.substring(0, fileStr.lastIndexOf('.'));
				fileStr += "."+eclipseTarget+ext;
				f = new File(fileStr);
				if (f.exists())
					resolvedUrl = f.toURL();
			}
		}
		
		parameter.addParameter("model", resolvedUrl.toExternalForm()); 
		
		GendocProcess gendocProcess = new GendocProcess();
		String resultFile = gendocProcess.runProcess(
				URIUtil.toURI(FileLocator.resolve(input)).toURL());		
		
		StringBuffer buf = new StringBuffer();
		int severity = diagnostician.getResultDiagnostic().getSeverity();
		if (severity != Diagnostic.OK) {
			for (Diagnostic d : diagnostician.getResultDiagnostic().getChildren()) {
				buf.append(d.getMessage()).append("\n");
			}
		}
		
		Assert.assertEquals(test+":" + buf.toString(), 
				Diagnostic.OK, 
				diagnostician.getResultDiagnostic().getSeverity());

		return new OpenOfficeVerifyHelper(resultFile,namespaceContext);
	}
}
