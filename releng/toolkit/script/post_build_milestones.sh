#!/bin/bash -x
milestone=$1
profile=$2
p2UpdateSite=$WORKSPACE/releng/org.eclipse.gendoc.update-site/target
updateSite=$WORKSPACE
downloadArea=/home/data/httpd/download.eclipse.org/gendoc
milestoneDownloadTarget=$downloadArea/updates/milestones/0.7.2/$milestone/$profile
milestoneDropsTarget=$downloadArea/milestones/0.7.2/$milestone/$profile

rm -rf $updateSite/repository
rm -rf $updateSite/*.zip
mkdir -p $updateSite/repository
mv $p2UpdateSite/repository $updateSite
mv $p2UpdateSite/*.zip $updateSite

ssh genie.gendoc@projects-storage.eclipse.org rm -rf $milestoneDownloadTarget
ssh genie.gendoc@projects-storage.eclipse.org mkdir -p $milestoneDownloadTarget
scp -r $updateSite/repository/* genie.gendoc@projects-storage.eclipse.org:$milestoneDownloadTarget/

ssh genie.gendoc@projects-storage.eclipse.org rm -rf $milestonDropsTarget
ssh genie.gendoc@projects-storage.eclipse.org mkdir -p $milestoneDropsTarget
scp -r $updateSite/*.zip genie.gendoc@projects-storage.eclipse.org:$milestoneDropsTarget/
