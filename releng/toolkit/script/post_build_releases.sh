#!/bin/bash -x
profile=$1
p2UpdateSite=$WORKSPACE/releng/org.eclipse.gendoc.update-site/target
updateSite=$WORKSPACE
downloadArea=/home/data/httpd/download.eclipse.org/gendoc
releaseDownloadTarget=$downloadArea/updates/releases/0.7.2/$profile
releaseDropsTarget=$downloadArea/releases/0.7.2/$profile

rm -rf $updateSite/repository
rm -rf $updateSite/*.zip
mkdir -p $updateSite/repository
mv $p2UpdateSite/repository $updateSite
mv $p2UpdateSite/*.zip $updateSite

ssh genie.gendoc@projects-storage.eclipse.org rm -rf $releaseDownloadTarget
ssh genie.gendoc@projects-storage.eclipse.org mkdir -p $releaseDownloadTarget
scp -r $updateSite/repository/* genie.gendoc@projects-storage.eclipse.org:$releaseDownloadTarget/

ssh genie.gendoc@projects-storage.eclipse.org rm -rf $milestonDropsTarget
ssh genie.gendoc@projects-storage.eclipse.org mkdir -p $releaseDropsTarget
scp -r $updateSite/*.zip genie.gendoc@projects-storage.eclipse.org:$releaseDropsTarget/
